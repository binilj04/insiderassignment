package com.biniljacob.insiderassignment.Repository;

import com.biniljacob.insiderassignment.ui.Pages.EventList.Event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DataFilter {





    public List<String> getGroupEvent(JSONObject jsonObj,String GroupName){

        JSONObject onjList  = jsonObj.optJSONObject("list");
        JSONObject Eventlist_GROUP = onjList.optJSONObject("groupwiseList");
        if(Eventlist_GROUP == null){
            return  null;
        }

        JSONArray arr = Eventlist_GROUP.optJSONArray(GroupName);
        if(arr == null){
            return null;
        }

        List<String> list = new ArrayList<String>();
        for(int j = 0; j < arr.length(); j++){
            list.add(arr.optString(j));
        }

        return list;
    }


    public List<String> getAllEventNames(JSONObject jsonObj){
        JSONObject onjList  = jsonObj.optJSONObject("list");
        JSONObject Eventlist_FULL = onjList.optJSONObject("masterList");
        List<String> list = new ArrayList<String>();
        for (int i = 0; i < Eventlist_FULL.length(); i++) {
            try {
                list.add((Eventlist_FULL.names().get(i).toString()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return list;

    }


    public Event getEvent(JSONObject jsonObj, String EventName){

        JSONObject onjList  = jsonObj.optJSONObject("list");
        JSONObject Eventlist_FULL = onjList.optJSONObject("masterList");
        JSONObject ev = Eventlist_FULL.optJSONObject(EventName);

        String img_url = ev.optString("horizontal_cover_image");
        String slug = ev.optString("slug");
        String name = ev.optString("name");
        String date = ev.optString("venue_date_string");
        String venue = ev.optString("venue_name");

        String price = ev.optString("price_display_string");
        int min_price = ev.optInt("min_price");

        String group = ev.optJSONObject("group_id").optString("name");
        String category = ev.optJSONObject("category_id").optString("name");
        float popularity_score = (float) ev.optDouble("popularity_score");

        Event loadEvent =  new Event(img_url,slug,name,date,venue,price,min_price,group,category,popularity_score);

        return loadEvent;
    }


    public List<String> getGroupNames(JSONObject jsonObj){
        JSONArray groups = jsonObj.optJSONArray("groups");
        List<String> Groups =  new ArrayList<String>();
        Groups.add("All");
        for (int i = 0; i < groups.length(); i++) {

            try {
                Groups.add(String.valueOf(groups.get(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return Groups;
    }






}
