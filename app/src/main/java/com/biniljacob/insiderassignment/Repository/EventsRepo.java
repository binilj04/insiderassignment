package com.biniljacob.insiderassignment.Repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.biniljacob.insiderassignment.Network.NetworkAPI;
import com.biniljacob.insiderassignment.ui.Pages.EventList.Event;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EventsRepo {
    
    private static EventsRepo instance;

    private MutableLiveData<Map<String,Boolean>> NetworkStatus = new MutableLiveData<>();
    private MutableLiveData<JSONObject> APIResponse = new MutableLiveData<>();
    private ArrayList<Event> dataSet = new ArrayList<>();
    String TAG = "EventsRepo";

    Map<String, Boolean> netStatus  = new HashMap<String, Boolean>() {{
        put("isLoading", false);
        put("isError", false);
    }};


    public static EventsRepo getInstance(){
        if(instance == null){
            instance = new EventsRepo();
        }
        return instance;
    }


    public void getDataFromAPI(String API_URL,String Location){

        netStatus.put("isLoading", true);
        netStatus.put("isError", false);
        NetworkStatus.postValue(netStatus);

        NetworkAPI sampleClass = new NetworkAPI();
        sampleClass.setNetworkAPIListener(API_URL, Location, new NetworkAPI.NetworkAPIListener() {
            @Override
            public void onDataLoaded( JSONObject jsonObj) {
                netStatus.put("isLoading", false);
                netStatus.put("isError", false);
                NetworkStatus.postValue(netStatus);
                APIResponse.postValue(jsonObj);
            }

            @Override
            public void onErrorLoading(String Error) {
                netStatus.put("isLoading", false);
                netStatus.put("isError", true);
                NetworkStatus.postValue(netStatus);

            }
        });
    }

    public LiveData<JSONObject> getResponse() {
        return APIResponse;
    }

    public LiveData<Map<String,Boolean>> RequestStatus() {
        return NetworkStatus;
    }




}
