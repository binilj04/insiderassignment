package com.biniljacob.insiderassignment;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.biniljacob.insiderassignment.Repository.DataFilter;
import com.biniljacob.insiderassignment.Repository.EventsRepo;
import com.biniljacob.insiderassignment.ui.Pages.SectionsPagerAdapter;

import org.json.JSONObject;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

public class MainActivity extends AppCompatActivity {

    Context mContext;
    DataFilter dataFilter = new DataFilter();
    private ProgressBar mProgressBar;

    private static final String TAG = "Main Activity";
    private static final OkHttpClient client = new OkHttpClient().newBuilder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build();

    String API_URL = "https://api.insider.in/home";
    String Location = "mumbai";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext =  this;
        setContentView(R.layout.activity_main);
        mProgressBar = findViewById(R.id.progress_bar);

        ViewPager viewPager = findViewById(R.id.view_pager);
        TabLayout tabs = findViewById(R.id.tabs);


        EventsRepo Repo = EventsRepo.getInstance();
        Repo.getDataFromAPI(API_URL, Location);

        Repo.getResponse().observe(this, new Observer<JSONObject>() {
            @Override
            public void onChanged(@Nullable JSONObject jsonObject) {

                SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(mContext,
                        getSupportFragmentManager(),dataFilter.getGroupNames(jsonObject));

                viewPager.setAdapter(sectionsPagerAdapter);
                tabs.setupWithViewPager(viewPager);

            }
        });

        Repo.RequestStatus().observe(this, new Observer<Map<String, Boolean>>() {
            @Override
            public void onChanged(@Nullable Map<String, Boolean> stringBooleanMap) {
                if(stringBooleanMap.get("isLoading").booleanValue()){
                    mProgressBar.setVisibility(View.VISIBLE);
                }else{
                    mProgressBar.setVisibility(View.GONE);
                }

                if(stringBooleanMap.get("isError").booleanValue()){
                    final Snackbar snackBar = Snackbar.make(findViewById(android.R.id.content), "Check Internet Connection", Snackbar.LENGTH_INDEFINITE);

                    snackBar.setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Call your action method here
                            Repo.getDataFromAPI(API_URL, Location);
                            snackBar.dismiss();
                        }
                    });
                    snackBar.show();
                }else{

                }


            }
        });





        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

            }
        });
    }




}

