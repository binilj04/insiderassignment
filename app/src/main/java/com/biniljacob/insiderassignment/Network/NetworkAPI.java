package com.biniljacob.insiderassignment.Network;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class NetworkAPI {

    private static final String TAG = "NetworkAPI";
    private static final OkHttpClient client = new OkHttpClient().newBuilder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build();

    public interface NetworkAPIListener {

        void onDataLoaded(JSONObject jsonObj);
        void onErrorLoading(String Error);
    }


    private NetworkAPIListener listener;

    public void setNetworkAPIListener(String URL, String City, NetworkAPIListener listener) {
        this.listener = listener;
        new NetworkThread().execute(URL,City);
    }



    class NetworkThread extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... arg0) {

            HttpUrl.Builder httpBuider = HttpUrl.parse(arg0[0]).newBuilder();
            httpBuider.addQueryParameter("norm","1");
            httpBuider.addQueryParameter("filterBy","go-out");
            httpBuider.addQueryParameter("city",arg0[1]);

            Request request = new Request.Builder().url(httpBuider.build()).build();
            Response response = null;
            String serverResponse = "";
            try {
                response = client.newCall(request).execute();
                serverResponse = response.body().string();
            } catch (IOException e) {
                e.printStackTrace();

            }

            JSONObject jsonObj;
            try {
                jsonObj = new JSONObject(serverResponse);

                listener.onDataLoaded(jsonObj);
            } catch (JSONException e) {
                e.printStackTrace();
                listener.onErrorLoading("Error Getting data");
            }

            return  "Network CMD Executed";
        }

        protected void onPostExecute(String result) {
            // TODO: do something with the feed
            Log.i(TAG, "onPostExecute: "+result);

        }
    }



}
