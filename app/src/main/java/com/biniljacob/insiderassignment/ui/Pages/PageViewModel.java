package com.biniljacob.insiderassignment.ui.Pages;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.biniljacob.insiderassignment.Repository.DataFilter;
import com.biniljacob.insiderassignment.Repository.EventsRepo;
import com.biniljacob.insiderassignment.ui.Pages.EventList.Event;

import java.util.ArrayList;
import java.util.List;

public class PageViewModel extends ViewModel {

    EventsRepo Repo = EventsRepo.getInstance();
    DataFilter dataFilter = new DataFilter();
    String TAG = "PageViewModel";
    String Title = "";
    private MutableLiveData<Integer> mIndex = new MutableLiveData<>();

//    private MutableLiveData<List<Event>> EventList = new MutableLiveData<>();

    private LiveData<List<Event>> EventList = Transformations.map(mIndex, new Function<Integer, List<Event>>() {
        @Override
        public List<Event> apply(Integer input) {
            List<String> EventNames;
            if(Title.toLowerCase().equals("all")){
                EventNames= dataFilter.getAllEventNames(Repo.getResponse().getValue());
            }else{
                EventNames= dataFilter.getGroupEvent(Repo.getResponse().getValue(),Title);
            }


            List<Event> list = new ArrayList<>();
            for (int i = 0; i < EventNames.size(); i++) {

                list.add(dataFilter.getEvent(Repo.getResponse().getValue(),EventNames.get(i)));

            }


            return list;

        }
    });



    public void setIndexAndName(int index, String title) {
        Title = title;
        mIndex.setValue(index);
    }
//
//    public LiveData<String> getText() {
//        return mText;
//    }


    public LiveData<List<Event>> getEventList() {
        return EventList;
    }
}