package com.biniljacob.insiderassignment.ui.Pages.EventList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.biniljacob.insiderassignment.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by guendouz on 15/02/2018.
 */

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventViewHolder> {

    public interface OnDeleteButtonClickListener {
        void onDeleteButtonClicked(Event Event);
    }

    private List<Event> data;
    private Context context;
    private LayoutInflater layoutInflater;
    private OnDeleteButtonClickListener onDeleteButtonClickListener;

    public EventsAdapter(Context context, OnDeleteButtonClickListener listener) {
        this.data = new ArrayList<>();
        this.context = context;
        this.onDeleteButtonClickListener = listener;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.layout_event_item, parent, false);
        return new EventViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        if(data == null){
            return 0;
        }
        return data.size();
    }

    public void setData(List<Event> newData) {
        data = newData;
    }

    class EventViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle, Venue,tvCategory,tvDate;
        private ImageView eventImage;


        EventViewHolder(View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            Venue = itemView.findViewById(R.id.venue);
            tvCategory  = itemView.findViewById(R.id.tvCategory);
            tvDate = itemView.findViewById(R.id.tvDate);
            eventImage =  itemView.findViewById(R.id.image);

        }

        void bind(final Event event) {
            if (event != null) {
                tvTitle.setText(event.getName());
                Venue.setText(event.getVenue());
                tvCategory.setText(event.getCategory().toUpperCase());
                tvDate.setText(event.getDate());
                Glide.with(context).load(event.getImg_url()).centerCrop().
                        transition(withCrossFade()).into(eventImage);


            }
        }

    }

//    class EventDiffCallback extends DiffUtil.Callback {
//
//        private final List<Event> oldEvents, newEvents;
//
//        public EventDiffCallback(List<Event> oldEvents, List<Event> newEvents) {
//            this.oldEvents = oldEvents;
//            this.newEvents = newEvents;
//        }
//
//        @Override
//        public int getOldListSize() {
//            return oldEvents.size();
//        }
//
//        @Override
//        public int getNewListSize() {
//            return newEvents.size();
//        }
//
//        @Override
//        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
//            return oldEvents.get(oldItemPosition).getId() == newEvents.get(newItemPosition).getId();
//        }
//
//        @Override
//        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
//            return oldEvents.get(oldItemPosition).equals(newEvents.get(newItemPosition));
//        }
//    }
}
