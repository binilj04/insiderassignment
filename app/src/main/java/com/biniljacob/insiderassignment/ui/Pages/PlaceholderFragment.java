package com.biniljacob.insiderassignment.ui.Pages;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.annotation.Nullable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;


import com.biniljacob.insiderassignment.R;
import com.biniljacob.insiderassignment.ui.Pages.EventList.Event;
import com.biniljacob.insiderassignment.ui.Pages.EventList.EventsAdapter;

import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment implements EventsAdapter.OnDeleteButtonClickListener {

    private EventsAdapter eventsAdapter;
//    private EventViewModel eventViewModel;

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_SECTION_NAME = "section_name";

    private PageViewModel pageViewModel;

    public static PlaceholderFragment newInstance(int index, String title) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        bundle.putString(ARG_SECTION_NAME, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 0;
        String title = "";
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
            title = getArguments().getString(ARG_SECTION_NAME);
        }
        pageViewModel.setIndexAndName(index,title);



    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);
//        final TextView textView = root.findViewById(R.id.section_label);
//        pageViewModel.getText().observe(this, new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });


        eventsAdapter = new EventsAdapter(getContext(), this);

//        eventViewModel = ViewModelProviders.of(this).get(EventViewModel.class);
////        eventViewModel.getAllevents().observe(this, events -> eventsAdapter.setData(events));
//
////        eventsAdapter.setData(eventViewModel.getAllEvents());

        pageViewModel.getEventList().observe(this, new Observer<List<Event>>() {
            @Override
            public void onChanged(@Nullable List<Event> events) {
                eventsAdapter.setData(pageViewModel.getEventList().getValue());
                eventsAdapter.notifyDataSetChanged();
            }
        });




        RecyclerView recyclerView = root.findViewById(R.id.rvEventsList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(eventsAdapter);



        return root;
    }

    @Override
    public void onDeleteButtonClicked(Event event) {

    }
}