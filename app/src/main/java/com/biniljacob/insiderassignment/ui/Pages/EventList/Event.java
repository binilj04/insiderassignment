package com.biniljacob.insiderassignment.ui.Pages.EventList;

public class Event {



    private  String img_url;
    private  String slug;
    private  String name;
    private  String date;
    private  String venue;
    private  String price;
    private int min_price;
    private  String group;
    private  String category;

    private  float popularity_score;

    public Event(){

    }

    public Event(String img_url,String slug, String name, String date, String venue, String price,int min_price, String group, String category, float popularity_score) {
        this.img_url = img_url;
        this.slug = slug;
        this.name = name;
        this.date = date;
        this.venue = venue;
        this.price = price;
        this.min_price = min_price;
        this.group = group;
        this.category = category;
        this.popularity_score = popularity_score;
    }

    public String getImg_url() {
        if(img_url.contains("c_crop,g_custom/")){
            return img_url.replace("c_crop,g_custom/", "");
        }
        return img_url;
    }

    public String getSlug() {
        return slug;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public String getVenue() {
        return venue;
    }

    public String getPrice() {
        return price;
    }

    public int getMinPrice() {
        return min_price;
    }

    public String getGroup() {
        return group;
    }

    public String getCategory() {
        return category;
    }

    public float getPopularity_score() {
        return popularity_score;
    }



}
